module Main where

import           System.Environment (getArgs)
import           Text.Megaparsec    (runParser)

import           Day1

main :: IO ()
main = do
  (filename:args) <- getArgs
  contents <- readFile filename
  case runParser inputParser filename contents of
    Left e -> print e
    Right input -> case args of
      ("--find-repeated":_) -> print $ calculateDistanceFirstRepeated input
      _ -> print $ calculateDistance input
