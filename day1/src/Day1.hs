module Day1 where

import           Data.Bifunctor
import           Data.List
import           Text.Megaparsec
import           Text.Megaparsec.String

inputParser :: Parser [Instruction]
inputParser = instruction `sepBy` (char ',' >> space)

calculateDistance :: [Instruction] -> Int
calculateDistance = go initialPosition
  where
    go pos [] = taxiDistanceFromOrigin $ coordinates pos
    go pos (i:is) = go (runInstruction i pos) is

calculateDistanceFirstRepeated :: [Instruction] -> Maybe Int
calculateDistanceFirstRepeated = go initialPosition []
  where
    go pos visited [] = Nothing
    go pos visited (i:is)
      = let willVisit = pointsIn i pos
            previouslyVisited = intersect visited willVisit
        in case previouslyVisited of
          [] -> go (runInstruction i pos) (visited ++ willVisit) is
          (x:_) -> Just $ taxiDistanceFromOrigin x

data Direction = L | R deriving (Eq, Show)

data Instruction = I { direction :: Direction, distance :: Int }
  deriving (Eq, Show)

data Orientation = N | S | E | W deriving (Eq, Show)

data Position = Position { orientation :: Orientation, coordinates :: (Int,Int) }
  deriving (Eq, Show)

initialPosition = Position N (0,0)

turn :: Direction -> Orientation -> Orientation
turn L N = W
turn L S = E
turn L E = N
turn L W = S
turn R N = E
turn R S = W
turn R E = S
turn R W = N

move :: Int -> Position -> Position
move dist pos
  = let (x,y) = coordinates pos
    in case orientation pos of
      N -> pos { coordinates = (x,y+dist) }
      S -> pos { coordinates = (x,y-dist) }
      E -> pos { coordinates = (x+dist,y) }
      W -> pos { coordinates = (x-dist,y) }

runInstruction :: Instruction -> Position -> Position
runInstruction instr pos
  = move (distance instr) $ pos { orientation = turn (direction instr) (orientation pos) }

taxiDistanceFromOrigin :: (Int,Int) -> Int
taxiDistanceFromOrigin (x,y) = abs x + abs y

pointsIn :: Instruction -> Position -> [(Int,Int)]
pointsIn i pos
  = let f = case turn (direction i) (orientation pos) of
          N -> second succ
          S -> second pred
          E -> first succ
          W -> first pred
    in take (distance i) . iterate f $ coordinates pos

instruction :: Parser Instruction
instruction = l <|> r
  where
    positive = read <$> some digitChar
    l = do
      char 'L'
      I L <$> positive
    r = do
      char 'R'
      I R <$> positive
