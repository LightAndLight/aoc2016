import           Test.Hspec
import           Text.Megaparsec (runParser)

import           Day1

main = hspec $ do
  describe "calculateDistance" $ do
    it "calculates the taxicab distance from the origin given a list of instructions" $ do
      calculateDistance [I R 2, I L 3] `shouldBe` 5
      calculateDistance [I R 2, I R 2, I R 2] `shouldBe` 2
      calculateDistance [I R 5, I L 5, I R 5, I R 3] `shouldBe` 12
  describe "calculateDistanceFirstRepeated" $ do
    it "calulates the taxicab distance from the origin of the first point to be visited twice" $ do
      calculateDistanceFirstRepeated [I R 8, I R 4, I R 4, I R 8] `shouldBe` Just 4
    it "will return nothing if no point was visited twice" $ do
      calculateDistanceFirstRepeated [I L 2, I R 2, I L 2] `shouldBe` Nothing
  describe "inputParser" $ do
    it "parses string of format (L|R)\\d+(,\\w*(L|R)\\d+)* into a list of instructions" $ do
      runParser inputParser "test" "R2" `shouldBe` Right [I R 2]
      runParser inputParser "test" "R2, L1, R10, R9" `shouldBe` Right [I R 2, I L 1, I R 10, I R 9]
