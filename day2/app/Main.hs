module Main where

import           System.Environment (getArgs)
import           System.IO          (readFile)

import           Day2

main = do
  (filename:args) <- getArgs
  contents <- lines <$> readFile filename
  let directions = traverse parseLine contents
  print $ case args of
    "--diamond":_ -> fmap showKey <$> (runLines <$> directions :: Maybe [DiamondKey])
    _ -> fmap showKey <$> (runLines <$> directions :: Maybe [Key])
