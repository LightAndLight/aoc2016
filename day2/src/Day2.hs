module Day2 where

import           Data.Foldable

data Row = Top | Middle | Bottom
data Column = LeftCol | CenterCol | RightCol
data Key = Key { row :: Row, column :: Column }

data Direction = U | D | L | R

class KeypadKey a where
  startKey :: a
  move :: a -> Direction -> a
  showKey :: a -> Char

instance KeypadKey Key where
  startKey = Key Middle CenterCol

  move (Key Middle c) U = Key Top c
  move (Key Bottom c) U = Key Middle c
  move (Key Top c) D = Key Middle c
  move (Key Middle c) D = Key Bottom c
  move (Key r CenterCol) L = Key r LeftCol
  move (Key r RightCol) L = Key r CenterCol
  move (Key r LeftCol) R = Key r CenterCol
  move (Key r CenterCol) R = Key r RightCol
  move k _ = k

  showKey (Key Top LeftCol) = '1'
  showKey (Key Top CenterCol) = '2'
  showKey (Key Top RightCol) = '3'
  showKey (Key Middle LeftCol) = '4'
  showKey (Key Middle CenterCol) = '5'
  showKey (Key Middle RightCol) = '6'
  showKey (Key Bottom LeftCol) = '7'
  showKey (Key Bottom CenterCol) = '8'
  showKey (Key Bottom RightCol) = '9'

data DiamondKey = DiamondKey { x :: Int, y :: Int }

canGoUp (DiamondKey 2 0) = False
canGoUp (DiamondKey 1 1) = False
canGoUp (DiamondKey 0 2) = False
canGoUp (DiamondKey (-1) 1) = False
canGoUp (DiamondKey (-2) 0) = False
canGoUp _ = True

canGoDown (DiamondKey 2 0) = False
canGoDown (DiamondKey 1 (-1)) = False
canGoDown (DiamondKey 0 (-2)) = False
canGoDown (DiamondKey (-1) (-1)) = False
canGoDown (DiamondKey (-2) 0) = False
canGoDown _ = True

canGoLeft (DiamondKey 0 2) = False
canGoLeft (DiamondKey (-1) 1) = False
canGoLeft (DiamondKey (-2) 0) = False
canGoLeft (DiamondKey (-1) (-1)) = False
canGoLeft (DiamondKey 0 (-2)) = False
canGoLeft _ = True

canGoRight (DiamondKey 0 2) = False
canGoRight (DiamondKey 1 1) = False
canGoRight (DiamondKey 2 0) = False
canGoRight (DiamondKey 1 (-1)) = False
canGoRight (DiamondKey 0 (-2)) = False
canGoRight _ = True

instance KeypadKey DiamondKey where
  startKey = DiamondKey (-2) 0

  move k U
    | canGoUp k = k { y = y k + 1 }
    | otherwise = k
  move k D
    | canGoDown k = k { y = y k - 1 }
    | otherwise = k
  move k L
    | canGoLeft k = k { x = x k - 1 }
    | otherwise = k
  move k R
    | canGoRight k = k { x = x k + 1 }
    | otherwise = k

  showKey (DiamondKey 0 2) = '1'
  showKey (DiamondKey (-1) 1) = '2'
  showKey (DiamondKey 0 1) = '3'
  showKey (DiamondKey 1 1) = '4'
  showKey (DiamondKey (-2) 0) = '5'
  showKey (DiamondKey (-1) 0) = '6'
  showKey (DiamondKey 0 0) = '7'
  showKey (DiamondKey 1 0) = '8'
  showKey (DiamondKey 2 0) = '9'
  showKey (DiamondKey (-1) (-1)) = 'A'
  showKey (DiamondKey 0 (-1)) = 'B'
  showKey (DiamondKey 1 (-1)) = 'C'
  showKey (DiamondKey 0 (-2)) = 'D'
  showKey _ = error "Key does not exist"

runLine :: KeypadKey k => [Direction] -> k -> k
runLine = flip (foldl' move)

runLines :: KeypadKey k => [[Direction]] -> [k]
runLines = go startKey . fmap runLine
  where
    go _ [] = []
    go k (f:fs) = let k' = f k in k' : go k' fs

parseDirection :: Char -> Maybe Direction
parseDirection 'U' = Just U
parseDirection 'D' = Just D
parseDirection 'L' = Just L
parseDirection 'R' = Just R
parseDirection _ = Nothing

parseLine :: String -> Maybe [Direction]
parseLine = traverse parseDirection
