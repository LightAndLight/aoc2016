import           Test.Hspec

import           Day2

main = hspec $ do
  describe "move" $ do
    it "moves in a direction from one key to another" $ do
      showKey (move (Key Bottom RightCol) L :: Key) `shouldBe` '8'
      showKey (move (Key Bottom CenterCol) U :: Key) `shouldBe` '5'
      showKey (move (Key Middle CenterCol) R :: Key) `shouldBe` '6'
      showKey (move (Key Middle RightCol) D :: Key) `shouldBe` '9'
  describe "runLine" $ do
    it "runs the directions on a keypad, starting at the specified key" $ do
      showKey (runLine [U,L,L] (Key Middle CenterCol) :: Key) `shouldBe` '1'
      showKey (runLine [R,R,D,D,D] (Key Top LeftCol) :: Key) `shouldBe` '9'
      showKey (runLine [L,U,R,D,L] (Key Bottom RightCol) :: Key) `shouldBe` '8'
      showKey (runLine [U,U,U,U,D] (Key Bottom CenterCol) :: Key) `shouldBe` '5'
  describe "runLines" $ do
    it "runs the directions on a keypad. starts at the default key, records the key it was on at the end of each line, uses that key to run the next line" $ do
      showKey <$> (runLines [[U,L,L],[R,R,D,D,D],[L,U,R,D,L],[U,U,U,U,D]] :: [Key]) `shouldBe` "1985"
      showKey <$> (runLines [[U,L,L],[R,R,D,D,D],[L,U,R,D,L],[U,U,U,U,D]] :: [DiamondKey]) `shouldBe` "5DB3"
