module Main where

import           Day21
import           System.Environment (getArgs)
import           Text.Megaparsec    (runParser)

main :: IO ()
main = do
  (filepath:args) <- getArgs
  contents <- readFile filepath
  let insts = runParser instructions filepath contents
  print $ case args of
    [] -> flip scramble "abcdefgh" <$> insts
    "--unscramble":_ -> flip unscramble "fbgdceah" <$> insts
