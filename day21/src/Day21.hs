{-# language Rank2Types #-}

module Day21 where

import Control.Monad.ST
import Data.Foldable
import Data.Array
import Control.Applicative
import Control.Monad
import Data.STRef
import Data.Array.ST
import Data.Array.MArray
import Data.List
import Text.Megaparsec
import Text.Megaparsec.String

data Instruction
  = SwapPos Int Int
  | SwapLetter Char Char
  | RotateLeft Int
  | RotateRight Int
  | RotateGiven Char
  | Reverse Int Int
  | Move Int Int
  deriving (Eq, Show)

instructions :: Parser [Instruction]
instructions = sepEndBy1 instruction eol <* eof

instruction :: Parser Instruction
instruction = swap <|> rotate <|> move <|> reverseSlice

swap :: Parser Instruction
swap = string "swap" *> spaceChar *> (swapPos <|> swapLetter)

posInt :: Parser Int
posInt = read <$> some digitChar

swapPos :: Parser Instruction
swapPos = do
  string "position" *> spaceChar
  from <- posInt <* spaceChar
  string "with position" *> spaceChar
  to <- posInt
  return $ SwapPos from to

swapLetter :: Parser Instruction
swapLetter = do
  string "letter" *> spaceChar
  from <- letterChar <* spaceChar
  string "with letter" *> spaceChar
  to <- letterChar
  return $ SwapLetter from to

rotate :: Parser Instruction
rotate = string "rotate" *> spaceChar *> (left <|> right <|> given)

left :: Parser Instruction
left = string "left" *> spaceChar *> (RotateLeft <$> posInt) <* spaceChar <* string "step" <* try (optional (char 's'))

right :: Parser Instruction
right = string "right" *> spaceChar *> (RotateRight <$> posInt) <* spaceChar <* string "step" <* try (optional (char 's'))

given :: Parser Instruction
given = string "based on position of letter" *> spaceChar *> (RotateGiven <$> letterChar)

move :: Parser Instruction
move = do
  string "move position" *> spaceChar
  from <- posInt <* spaceChar
  string "to position" *> spaceChar
  to <- posInt
  return $ Move from to

reverseSlice :: Parser Instruction
reverseSlice = do
  string "reverse positions" *> spaceChar
  from <- posInt <* spaceChar
  string "through" *> spaceChar
  to <- posInt
  return $ Reverse from to

indexOf :: (MArray a e m, Ix i, Eq e) => e -> a i e -> m (Maybe i)
indexOf e array = do
  assocs <- getAssocs array
  return $ fst <$> find ((==) e . snd) assocs

posMod :: Int -> Int -> Int
posMod n m
  | n >= 0 = n `mod` m
  | otherwise = -(n `div` m) * m + n

-- | Rotate each element of the array n position to the right ie. negative is to the left
rotateSlice :: Int -> Int -> Int -> STArray s Int Char -> ST s (STArray s Int Char)
rotateSlice _ _ 0 array = pure array
rotateSlice from to d array = do
  let size = rangeSize (from,to)
  let bucketSize = gcd size d
  let numBuckets = size `div` bucketSize
  zero <- readArray array from
  temp <- newSTRef zero
  for_ [0..bucketSize - 1] $ \n -> do
    readArray array (n+from) >>= writeSTRef temp
    for_ [n,n+d..n + numBuckets * d] $ \k -> do
      let currentPos = from + (k `posMod` size)
      currentElem <- readArray array currentPos
      stored <- readSTRef temp
      writeArray array currentPos stored
      writeSTRef temp currentElem
  return array

runInstruction :: Instruction -> STArray s Int Char -> ST s (STArray s Int Char)
runInstruction (SwapPos x y) array = do
  xChar <- readArray array x
  yChar <- readArray array y
  writeArray array x yChar
  writeArray array y xChar
  return array
runInstruction (SwapLetter x y) array = do
  xIndex <- indexOf x array
  yIndex <- indexOf y array
  case xIndex of
    Nothing -> pure array
    Just xIndex -> case yIndex of
      Nothing -> pure array
      Just yIndex -> runInstruction (SwapPos xIndex yIndex) array
runInstruction (RotateLeft d) array = do
  (lower,upper) <- getBounds array
  rotateSlice lower upper (-d) array
runInstruction (RotateRight d) array = do
  (lower,upper) <- getBounds array
  rotateSlice lower upper d array
runInstruction (RotateGiven x) array = do
  xIndex <- indexOf x array
  case xIndex of
    Nothing -> pure array
    Just xIndex -> runInstruction (RotateRight (succ $ if xIndex >= 4 then xIndex + 1 else xIndex)) array
runInstruction (Reverse x y) array = do
  elems <- getBetween x y array
  for_ (zip [x..y] $ reverse elems) $ uncurry (writeArray array)
  return array
  where
    getBetween from to array
      | from > to = pure []
      | otherwise = do
        bounds <- getBounds array
        if from >= fst bounds
          then liftA2 (:) (readArray array from) $ getBetween (from + 1) to array
          else getBetween (from + 1) to array
runInstruction (Move x y) array
  | x == y = pure array
  | x < y = rotateSlice x y (-1) array
  | x > y = rotateSlice y x 1 array

unrunInstruction :: Instruction -> STArray s Int Char -> ST s (STArray s Int Char)
unrunInstruction (RotateLeft d) array = runInstruction (RotateRight d) array
unrunInstruction (RotateRight d) array = runInstruction (RotateLeft d) array
unrunInstruction (RotateGiven x) array = do
  xIndex <- indexOf x array
  case xIndex of
    Nothing -> pure array
    Just xIndex -> runInstruction (RotateLeft $ rotateAmount xIndex) array
  where
    rotateAmount n = case n of
      0 -> 1
      1 -> 1
      2 -> -2
      3 -> 2
      4 -> -1
      5 -> 3
      6 -> 0
      7 -> 4
unrunInstruction (Move x y) array = runInstruction (Move y x) array
unrunInstruction i array = runInstruction i array

toArray :: String -> (forall s. ST s (STArray s Int Char))
toArray input = newListArray (0,length input - 1) input

scrambleWith :: (forall s. Instruction -> STArray s Int Char -> ST s (STArray s Int Char)) -> [Instruction] -> String -> String
scrambleWith f insts input = elems $ runSTArray scrambleWith'
  where
    scrambleWith' :: ST s (STArray s Int Char)
    scrambleWith' = do
      array <- toArray input
      go insts array

    go :: [Instruction] -> STArray s Int Char -> ST s (STArray s Int Char)
    go [] array = pure array
    go (i:is) array = do
      array' <- f i array
      go is array'

scramble :: [Instruction] -> String -> String
scramble = scrambleWith runInstruction

unscramble :: [Instruction] -> String -> String
unscramble = scrambleWith unrunInstruction . reverse
