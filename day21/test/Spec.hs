import           Control.Monad.ST
import           Data.Array
import           Data.Array.ST
import           Test.Hspec

import           Day21

runInstructionOn :: Instruction -> String -> String
runInstructionOn inst input = elems $ runSTArray action
  where
    action :: ST s (STArray s Int Char)
    action = toArray input >>= runInstruction inst

unrunInstructionOn :: Instruction -> String -> String
unrunInstructionOn inst input = elems $ runSTArray action
  where
    action :: ST s (STArray s Int Char)
    action = toArray input >>= unrunInstruction inst

runRotateSlice :: Int -> Int -> Int -> String -> String
runRotateSlice from to amount input = elems $ runSTArray action
  where
    action :: ST s (STArray s Int Char)
    action = toArray input >>= rotateSlice from to amount

main :: IO ()
main = hspec $ do
  describe "rotateSlice" $ do
    it "rotates the letters in a given slice" $ do
      runRotateSlice 0 4 (-1) "abcde" `shouldBe` "bcdea"
      runRotateSlice 1 4 (-1) "abcde" `shouldBe` "acdeb"
      runRotateSlice 1 3 (-1) "abcde" `shouldBe` "acdbe"
  describe "runInstruction" $ do
    it "swaps the letters at two indices" $ do
      runInstructionOn (SwapPos 4 0) "abcde" `shouldBe` "ebcda"
    it "swaps two letters in the string" $ do
      runInstructionOn (SwapLetter 'd' 'b') "ebcda" `shouldBe` "edcba"
    it "reverses a slice" $ do
      runInstructionOn (Reverse 0 4) "edcba" `shouldBe` "abcde"
    it "rotates the string by n to the left" $ do
      runInstructionOn (RotateLeft 1) "abcde" `shouldBe` "bcdea"
      runInstructionOn (RotateLeft 2) "abcde" `shouldBe` "cdeab"
    it "rotates the string by n to the right" $ do
      runInstructionOn (RotateRight 1) "abcde" `shouldBe` "eabcd"
      runInstructionOn (RotateRight 2) "abcde" `shouldBe` "deabc"
    it "moves a character by taking it out then putting it back in" $ do
      runInstructionOn (Move 1 4) "bcdea" `shouldBe` "bdeac"
      runInstructionOn (Move 3 0) "bdeac" `shouldBe` "abdec"
    it "rotates the string a certain number of times given the index of a character" $ do
      runInstructionOn (RotateGiven 'b') "abdec" `shouldBe` "ecabd"
      runInstructionOn (RotateGiven 'd') "ecabd" `shouldBe` "decab"
  describe "unrunInstruction" $ do
    it "reverses the effects of an instruction" $ do
      unrunInstructionOn (SwapPos 4 0) "ebcda" `shouldBe` "abcde"
      unrunInstructionOn (SwapLetter 'd' 'b') "edcba" `shouldBe` "ebcda"
      unrunInstructionOn (Reverse 0 4) "abcde" `shouldBe` "edcba"
      unrunInstructionOn (RotateLeft 1) "bcdea" `shouldBe` "abcde"
      unrunInstructionOn (RotateLeft 2) "cdeab" `shouldBe` "abcde"
      unrunInstructionOn (RotateRight 1) "eabcd" `shouldBe` "abcde"
      unrunInstructionOn (RotateRight 2) "deabc" `shouldBe` "abcde"
      unrunInstructionOn (Move 1 4) "bdeac" `shouldBe` "bcdea"
      unrunInstructionOn (Move 3 0) "abdec" `shouldBe` "bdeac"
      unrunInstructionOn (RotateGiven 'b') "ecabd" `shouldBe` "abdec"
      unrunInstructionOn (RotateGiven 'd') "decab" `shouldBe` "ecabd"
  describe "scramble" $ do
    it "scrambles a string given a list of instructions" $ do
      scramble
        [ SwapPos 4 0
        , SwapLetter 'd' 'b'
        , Reverse 0 4
        , RotateLeft 1
        , Move 1 4
        , Move 3 0
        , RotateGiven 'b'
        , RotateGiven 'd'
        ] "abcde" `shouldBe` "decab"
  describe "unscramble" $ do
    it "unscrambles a string by running a scramble sequence in reverse" $ do
      unscramble
        [ SwapPos 4 0
        , SwapLetter 'd' 'b'
        , Reverse 0 4
        , RotateLeft 1
        , Move 1 4
        , Move 3 0
        , RotateGiven 'b'
        , RotateGiven 'd'
        ] "decab" `shouldBe` "abcde"
