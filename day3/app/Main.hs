module Main where

import           System.Environment (getArgs)
import           System.IO          (readFile)

import           Day3

main = do
  filename:args <- getArgs
  let passThree f (a:b:c:_) = f a b c
  set <- fmap (fmap read . words) . lines <$> readFile filename
  print . length $ filter (passThree isTriangle) $ case args of
    "--vertical":_ -> groupsOf 3 $ transpose set
    _ -> set
