module Day3 where

import           Data.Bifunctor

isTriangle :: Int -> Int -> Int -> Bool
isTriangle a b c = a + b > c && b + c > a && a + c > b

transpose :: [[a]] -> [a]
transpose [] = []
transpose as
  = let (top,rest) = go as
    in top ++ transpose rest
  where
    go [] = ([],[])
    go ([]:as) = go as
    go ((a:as):rest) = bimap (a :) (as :) $ go rest

groupsOf :: Int -> [a] -> [[a]]
groupsOf n [] = []
groupsOf n as = let (a,rest) = splitAt n as in a : groupsOf n rest
