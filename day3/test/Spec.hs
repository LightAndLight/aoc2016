import           Test.Hspec

import           Day3

main = hspec $ do
  describe "isTriangle" $ do
    it "determines if the three numbers can make up the three sides of a triagle" $ do
      isTriangle 5 10 25 `shouldBe` False
  describe "transpose" $ do
    it "adds the head of each list inside a list to a new list until there are no elements left" $ do
      transpose [[1,2,3],[4,5,6],[7,8,9],[10,11,12]] `shouldBe` [1,4,7,10,2,5,8,11,3,6,9,12]
  describe "groupsOf" $ do
    it "splits a list into sized groups" $ do
      groupsOf 3 [1,2,3,4,5,6,7,8,9,10,11,12] `shouldBe` [[1,2,3],[4,5,6],[7,8,9],[10,11,12]]
