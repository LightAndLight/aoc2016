module Main where

import           Data.Either
import           System.Environment (getArgs)
import           System.IO          (readFile)
import           Text.Megaparsec    (runParser)

import           Day4

main = do
  (filename:args) <- getArgs
  content <- lines <$> readFile filename
  let rooms = fmap (filter isReal) . sequence $ fmap (runParser room filename) content
  case args of
    "--decrypt":_ -> either print putStrLn $ unlines . fmap (\r -> decryptRoom r ++ ": " ++ show (sectorId r)) <$> rooms
    _ -> print $ sum . fmap sectorId <$> rooms
