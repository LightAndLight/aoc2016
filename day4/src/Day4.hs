module Day4 where

import           Control.Applicative
import           Data.Foldable
import           Data.List
import           Data.Map               (Map)
import qualified Data.Map               as M
import           Data.Monoid
import           Data.Ord
import           Text.Megaparsec
import           Text.Megaparsec.String

data Room = Room { roomWords :: [String], sectorId :: Int, checksum :: [Char] }
  deriving (Eq, Show)

word :: Parser String
word = some lowerChar

number :: Parser Int
number = read <$> some digitChar

sepBy1L :: Parser a -> Parser b -> Parser [b]
sepBy1L sep item = liftA2 (:) item $ many (try $ sep *> item)

room :: Parser Room
room = Room <$> (sepBy1L (char '-') word <* char '-') <*> number <*> between (char '[') (char ']') word

letterCount :: String -> Map Char Int
letterCount = foldr (flip (M.insertWith (+)) 1) M.empty

letterCounts :: [String] -> Map Char Int
letterCounts = foldr (M.unionWith (+) . letterCount) M.empty

compareLetterCounts :: (Ord a, Ord b) => (a,b) -> (a,b) -> Ordering
compareLetterCounts a b
  = case comparing snd b a of
      EQ -> comparing fst a b
      c -> c

createChecksum :: [String] -> [Char]
createChecksum = take 5 . fmap fst . sortBy compareLetterCounts . M.toList . letterCounts

isReal :: Room -> Bool
isReal room = checksum room ==  createChecksum (roomWords room)

rotateChar :: Char -> Char
rotateChar 'z' = 'a'
rotateChar c = succ c

decryptChar :: Int -> Char -> Char
decryptChar n = appEndo (fold $ replicate n (Endo rotateChar))

decryptRoom :: Room -> String
decryptRoom r = unwords $ fmap (decryptChar (sectorId r)) <$> roomWords r
