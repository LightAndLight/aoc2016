import qualified Data.Map        as M
import           Test.Hspec
import           Text.Megaparsec (runParser)

import           Day4

main = hspec $ do
  describe "room" $ do
    it "parses a room from a line of text" $ do
      runParser room "test" "aaaaa-bbb-z-y-x-123[abxyz]" `shouldBe` Right (Room ["aaaaa","bbb","z","y","x"] 123 "abxyz")
      runParser room "test" "a-b-c-d-e-f-g-h-987[abcde]" `shouldBe` Right (Room ["a","b","c","d","e","f","g","h"] 987 "abcde")
      runParser room "test" "not-a-real-room-404[oarel]" `shouldBe` Right (Room ["not","a","real","room"] 404 "oarel")
  describe "letterCount" $ do
    it "counts the occurrences of letters in a word" $ do
      letterCount "aaaaa" `shouldBe` M.fromList [('a',5)]
      letterCount "not" `shouldBe` M.fromList [('n',1), ('o',1), ('t',1)]
      letterCount "room" `shouldBe` M.fromList [('r',1), ('o',2), ('m',1)]
  describe "letterCounts" $ do
    it "counts the occurrences of letters in a list of words" $ do
      letterCounts (words "aaaaabbbzyx") `shouldBe` M.fromList [('a',5), ('b',3), ('z',1), ('y',1), ('x',1)]
  describe "createChecksum" $ do
    it "creates a checksum for a given room" $ do
      createChecksum ["aaaaa","bbb","z","y","x"] `shouldBe` "abxyz"
      createChecksum ["a","b","c","d","e","f","g","h"] `shouldBe` "abcde"
      createChecksum ["not","a","real","room"] `shouldBe` "oarel"
  describe "isReal" $ do
    it "checks whether a room is real" $ do
      isReal (Room ["aaaaa","bbb","z","y","x"] 123 "abxyz") `shouldBe` True
      isReal (Room ["a","b","c","d","e","f","g","h"] 987 "abcde") `shouldBe` True
      isReal (Room ["not","a","real","room"] 404 "oarel") `shouldBe` True
      isReal (Room ["totally","real","room"] 404 "decoy") `shouldBe` False
  describe "decryptRoom" $ do
    it "decrypts a room" $ do
      decryptRoom (Room ["qzmt", "zixmtkozy", "ivhz"] 343 undefined) `shouldBe` "very encrypted name"
