module Main where

import           Criterion.Main

import           Day5

main :: IO ()
main = defaultMain
  [ bench "findPasswordConcurrently" . nfIO $ findPasswordConcurrently "abc"
  ]
