module Main where

import           System.Environment (getArgs)

import           Day5

main :: IO ()
main = do
  [input] <- getArgs
  findPasswordConcurrently input >>= putStrLn
  putStrLn $ findPassword' input
