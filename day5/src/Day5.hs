module Day5 where

import           Control.Concurrent.Async
import           Control.DeepSeq
import           Crypto.Hash.MD5
import           Data.ByteString          (ByteString)
import           Data.ByteString.Base16
import qualified Data.ByteString.Char8    as B
import           Data.Char
import           Data.List
import           Data.Ord
import           GHC.Conc.Sync

zeroes = B.pack "00000"

findPassword' :: String -> String
findPassword' doorId
  = let list = filter (\x -> digitToInt (fst x) <= 7) .
          fmap (\s -> (B.index s 0,B.index s 1)) .
          fmap (B.drop 5) .
          filter (B.isPrefixOf zeroes) $
          fmap (\n -> encode . hash . B.pack $ doorId ++ show n) [1..]
    in fmap snd .
      sortBy (comparing fst) $
      loop [] list
  where
    loop acc (l:ls)
      | fst l `notElem` fmap fst acc = loop (l : acc) ls
      | length acc < 8 = loop acc ls
      | otherwise = acc

findPasswordInRange :: String -> Int -> Int -> String
findPasswordInRange doorId from to
  = fmap (`B.index` 5) .
    filter (B.isPrefixOf zeroes) $
    fmap (\n -> encode . hash . B.pack $ doorId ++ show n) [from..to]

findPasswordConcurrently :: String -> IO String
findPasswordConcurrently doorId = do
  numThreads <- getNumCapabilities
  loop numThreads 0 numThreads ""
  where
    hashes = 1000000
    loop numThreads from to password = do
      res <- forConcurrently [from..to] $ \n ->
        return $!! findPasswordInRange doorId (n * hashes) (succ n * hashes - 1)
      let password' = password ++ concat res
      if length password' < 8
        then loop numThreads (to + 1) (to + numThreads) password'
        else return $ take 8 password'
