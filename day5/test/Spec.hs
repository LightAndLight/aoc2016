import           Test.Hspec

import           Day5

main = hspec $ do
  describe "findPasswordConcurrently" $ do
    it "finds the 8 digit passcode associated with a word asynchronously" $ do
      findPasswordConcurrently "abc" `shouldReturn` "18f47a30"
  describe "findPassword'" $ do
    it "finds the 8 digit passcode associated with a word by using positional information" $ do
      findPassword' "abc" `shouldBe` "05ace8e3"
