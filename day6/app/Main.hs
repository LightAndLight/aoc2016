module Main where

import           System.Environment (getArgs)

import           Day6

main :: IO ()
main = do
  [input] <- getArgs
  file <- readFile input
  print $ countChars One file
  print $ countChars Two file
