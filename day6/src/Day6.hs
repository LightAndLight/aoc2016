module Day6 where

import Data.Char

data Challenge = One | Two

countChars :: Challenge -> String -> Int
countChars _ [] = 0
countChars challenge (c:rest)
  | isSpace c = countChars challenge rest
  | c == '('
  , (chunkSizeStr, _:rest') <- break (=='x') rest
  , (numTimesStr, _:rest'') <- break (==')') rest'
  , let chunkSize = read chunkSizeStr
  , let numTimes = read numTimesStr
  , let factor =
          case challenge of
            One -> chunkSize
            Two -> countChars challenge (take chunkSize rest'')
  = numTimes * factor + countChars challenge (drop chunkSize rest'')

  | (str, rest') <- break (=='(') (c:rest)
  = length str + countChars challenge rest'
